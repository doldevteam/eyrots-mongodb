<?php
namespace Eyrots\Provider\Mongodb;

use \Pimple\ServiceProviderInterface;
use \Pimple\Container;

//use \MongoDB\MongoDB\Client;
use \Doctrine\ODM\MongoDB\Configuration;
use \Doctrine\ODM\MongoDB\DocumentManager;
use \Doctrine\ODM\MongoDB\Mapping\Driver\AnnotationDriver;
use \MongoDB\Client;

use \Eyrots\Reducer\ConfigReducer;

class MongodbProvider implements ServiceProviderInterface
{
  public function register(Container $container)
  {
    $this->instantiateMongodb($container);
  }
  protected function instantiateMongodb(Container $container)
  {
    //AnnotationDriver::registerAnnotationClasses();
    $database = $container['settings']['mongodb.client.database'] ?? null;
    $config = new Configuration();
    $config->setProxyDir($container['settings']['mongodb.proxy.directory']);
    $config->setProxyNamespace($container['settings']['mongodb.proxy.namespace']);
    $config->setHydratorDir($container['settings']['mongodb.hydrator.directory']);
    $config->setHydratorNamespace($container['settings']['mongodb.proxy.namespace']);
    $config->setMetadataDriverImpl(AnnotationDriver::create($container['settings']['mongodb.models']));
    if($database)
    {
      $config->setDefaultDb($database);
    }

    $servers = implode(',', $container['settings']['mongodb.client.servers'] ?? []);
    $type = $container['settings']['mongodb.client.type'] ?? 'mongodb';
    $auth = ($auth = sprintf(
      "%s:%s@",
      urlencode($container['settings']['mongodb.client.user'] ?? ''),
      urlencode($container['settings']['mongodb.client.pass'] ?? '')
    )) != ':@' ? $auth : '';
    $authDatabase = '/' . $container['settings']['mongodb.client.authDatabase'] ?? '';
    $options = [];
    if(null != ($ssl = $container['settings']['mongodb.client.ssl'] ?? null))
    {
      $options['ssl'] = $ssl ? 'true' : 'false';
    }
    if(($replicaSet = $container['settings']['mongodb.client.replicaSet'] ?? null))
    {
      $options['replicaSet'] = $replicaSet;
    }
    if(($authSource = $container['settings']['mongodb.client.authSource'] ?? null))
    {
      $options['authSource'] = $authSource;
    }
    if(null != ($retryWrites = $container['settings']['mongodb.client.retryWrites'] ?? null))
    {
      $options['retryWrites'] = $retryWrites ? 'true' : 'false';
    }

    $connectionString = sprintf("%s://%s%s%s?%s", $type, $auth, $servers, $authDatabase, http_build_query($options));

    $client = DocumentManager::create(new Client($connectionString, [], ['typeMap' => DocumentManager::CLIENT_TYPEMAP]), $config);
    $container['mongodb'] = $client;
  }
  public static function configurationFilters(Container $container)
  {
    ConfigReducer::addFilter('mongodb.proxy.directory', function($value) use($container){
      if(substr($value, 0, 1) != '/')
      {
        return $container['settings']['appbase'] . '/' . $value;
      }
      return $value;
    });
    ConfigReducer::addFilter('mongodb.hydrator.directory', function($value) use($container){
      if(substr($value, 0, 1) != '/')
      {
        return $container['settings']['appbase'] . '/' . $value;
      }
      return $value;
    });
    ConfigReducer::addFilter('mongodb.models', function($value) use($container){
      if(substr($value, 0, 1) != '/')
      {
        return $container['settings']['appbase'] . '/' . $value;
      }
      return $value;
    });
  }
}